#!/bin/bash

set -x

ssh -o "StrictHostKeyChecking no" travis@188.166.18.204 -p 18665 /bin/bash << EOF
  cd /var/www/saavia.com/api
  pm2 delete saavia.js -s
  rm -rf *
  rm -rf .*
  git clone https://gitlab.com/nulpas/saavia-api.git
  cd saavia-api
  yarn
  yarn build
  mv dist/* ../
  mv src/migrations ../
  mv node_modules ../
  mv package.json ../
  mv ecosystem.config.js ../
  cd ../
  rm -rf saavia-api swagger.yml
  mv index.js saavia.js
  mv index.js.map saavia.js.map
  export JWT_SECRET=${JWT_SECRET}
  export SALT_ROUNDS=${SALT_ROUNDS}
  export NODE_ENV=${NODE_ENV}
  yarn start:pro
EOF
